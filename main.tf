data "docker_registry_image" "Radarr" {
	name = "linuxserver/radarr:latest"
}

resource "docker_image" "Radarr" {
	name = data.docker_registry_image.Radarr.name
	pull_triggers = [data.docker_registry_image.Radarr.sha256_digest]
}

module "Radarr" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Radarr.latest

	networks = [{ name: var.docker_network, aliases: ["radarr.${var.internal_domain_base}"] }]
  ports = [{ internal: 7878, external: 9601, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Radarr"
			container_path = "/config"
			read_only = false
		},
		{
			host_path = "${var.movies}"
			container_path = "/movies"
			read_only = false
		},
		{
			host_path = "${var.downloads}"
			container_path = "/downloads"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}